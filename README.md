# DynamoDB Importer

Used to transfer data from one DynamoDB to another

Based on response in Stack Overflow: https://stackoverflow.com/questions/43607592/copy-dynamodb-table-to-another-aws-account-without-s3

## Use Directions
* The following environment variables need to be set (`export VARIABLE=variable_value`)
  * SOURCE_ROLE
  * TARGET_ROLE
  * SOURCE_TABLE
  * TARGET_TABLE_NAME
* Running
  * `python import.py`
