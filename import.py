"""Used to move data from one DynamoDB table to another."""

import os
import boto3

source_role_name = os.environ.get('SOURCE_ROLE')
target_role_name = os.environ.get('TARGET_ROLE')

print("Source Role Name: {}". format(source_role_name))
print("Target Role Name: {}". format(target_role_name))

sts_client = boto3.client('sts')

assume_response = sts_client.assume_role(RoleArn=source_role_name,
                                         RoleSessionName='sourcesession')
source_creds = assume_response["Credentials"]
source_client = boto3.client('dynamodb',
                             aws_access_key_id=source_creds['AccessKeyId'],
                             aws_secret_access_key=source_creds['SecretAccessKey'],
                             aws_session_token=source_creds['SessionToken'])

assume_response = sts_client.assume_role(RoleArn=target_role_name,
                                         RoleSessionName='sourcesession')
target_creds = assume_response["Credentials"]
target_client = boto3.client('dynamodb',
                             aws_access_key_id=target_creds['AccessKeyId'],
                             aws_secret_access_key=target_creds['SecretAccessKey'],
                             aws_session_token=target_creds['SessionToken'])


dynamo_paginator = source_client.get_paginator('scan')
source_table_name = os.environ.get('SOURCE_TABLE_NAME')
target_table_name = os.environ.get('TARGET_TABLE_NAME')

print("Source Table Name: {}". format(source_table_name))
print("Target Table Name: {}". format(target_table_name))

source_data = dynamo_paginator.paginate(TableName=source_table_name,
                                        Select='ALL_ATTRIBUTES',
                                        ReturnConsumedCapacity='NONE',
                                        ConsistentRead=True)

print("Source Data: {}". format(source_data))

for page in source_data:
    for item in page['Items']:
        target_client.put_item(TableName=target_table_name,
                               Item=item)
